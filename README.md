# README #

This public GIT repository 
[https://bitbucket.org/iso-reference-data/iso-ics](https://bitbucket.org/iso-reference-data/iso-ics) 
hosts the ISO ICS (International Classification for Standards)
in several machine-readable formats.

The [official publication](https://www.iso.org/publication/PUB100033.html) is a PDF 
document on the [ISO website](https://www.iso.org).

### Contents ###

* The master file is an OWL ontology that defines:
  * Entities that make the concepts of the ICS (classes, properties, relationships)
  * Constraints on properties and relationships
  * Individuals (the classification tree itself)

The ontology has 2 web addresses: 
1. The resource itself exposed by Bitbucket as raw content
2. A Permanent URL (currently et purl.org) that is used for the stable ontology IRI, and which
 redirects to the resource

The ontology IRI to use to always refer to the latest version is:
[https://purl.org/sdo-hapi/ontologies/ics](https://purl.org/sdo-hapi/ontologies/ics).

For the current Edition is 7 these two addresses are:
1. The resource is available as file 
   [ICS.owl](https://bitbucket.org/iso-reference-data/iso-ics/raw/ed7/data/ICS.owl)
2. The IRI is [https://purl.org/sdo-hapi/ontologies/ics/7](https://purl.org/sdo-hapi/ontologies/ics/7)

To your convenience we provide a CSV and simple XML+XSD equivalent derived format.
Those have been derived by a semi-manual procedure.


### How do I get set up? ###

* You may clone this public repository

### Contribution guidelines ###

Conversion from authoritative .owl file to .ttl format: `riot --output=TURTLE ICS.owl >ICS.ttl`

### Who do I talk to? ###

* For support please reach out to the [ISO Helpdesk](mailto:helpdesk@iso.org)
